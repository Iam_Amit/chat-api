const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const path = require("path");

const publicDirectoryPath = path.join(__dirname, "./modules/chat");
app.use(express.static(publicDirectoryPath));


/****  ROUTES OF API  ****/
const userRoute = require('./modules/user/route');
const usrChat = require('./modules/chat/route');
const usrGroup = require('./modules/group/route');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(userRoute);
app.use(usrChat);
app.use(usrGroup);


/****  CREATING SERVER FOR SOCKET API ****/
const http = require('http');
const server = http.createServer(app)

const io = require('socket.io')(server)
require("./websocket")(io)


server.listen("3000", () => {
    console.log(' The APP is running on port 3000');
});

// const port = 3000;


