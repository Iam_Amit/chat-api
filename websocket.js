const chatModel = require("./modules/chat/schema");
const chatRoomModel = require("./modules/group/schema");
const userModel = require("./modules/user/schema");

const connect = (io) => {
  io.on("connection",  (socket) => {


    // User is Online !
    socket.on("isOnline", (payload, cb) => {
      const id = payload.userId;

      let data = {
        isOnline: payload.isOnline,
        socketId: socket.id,
      };
      try {
        userModel
          .findByIdAndUpdate(id, data)
          .select("_id userName")
          .then((data) => {
            console.log("data", data);
            return cb(data);
          })
          .catch((err) => {
            console.log(err, "error");
            return cb(err);
          });
      } catch (error) {
        return cb(error);
      }

      io.emit("isOnline", "msg");
    });


    // User Offline
    socket.on("disconnect", () => {
      // console.log('User is Disconnected !',data);
      console.log("A user disconnected", socket.id);
      const query = { socketId: socket.id };
      console.log("Query => ", query);

      userModel.findOneAndUpdate(
        query,
        { $set: { isOnline: false, socketId: "" } },
        function (err, data) {}
      );
    });


    // Creating the group
    socket.on("createGroup", function (payload, cb) {
      console.log("Socket Id createGrp => ", socket.id);
        try { 
          let data = {
          name: payload.name,
          members: payload.members,
          type: payload.type,
    
          };

        if (payload.type === "single" && !payload.name) {
          console.log("first chat initiate with user = > ");
       
          chatRoomModel(data)
            .save()
            .then((data) => {
              return cb(data);
            })
            .catch((err) => {
              return cb(err);
            })
            .catch((error) => {
              return cb(error);
            });
        } else {
          console.log("chat already exist with user !");
        }
      } catch (error) {
        return cb(error);
      }
      io.emit("createGroup", "Room Created");
    });


    // Message Sent
    socket.on("sendMessage", async function (payload, ack) {
      console.log("Socket Id of sendMsg => ", socket.id);
      try { 
        const roomExist = await chatRoomModel.findById({_id: payload.room_id})
        console.log(roomExist)

        if(!roomExist) {
          console.log('No room exist with the id ')
        }
        else 
        {  
          let data = {
            from_user: payload.from_user,
            message: payload.message,
            // to_user: payload.to_user,
            room_id: payload.room_id,
          };

          console.log('Can not send the message to yourself  !');
          chatModel(data)
          .save()
          .then((data) => {
            console.log("data => ", data);
            return ack(data);
          })
          .catch((err) => {
            console.log(err, "error");
            return ack(err);
          });
        }
      } catch (err) {
        // return cb(err);
        console.log(err);
        socket.emit("error", { message: err });
      }
      io.broadcast.emit("sendMessage", "msg");
    });


    // received message
    socket.on("receivedMessage", function (payload, cb) {
      try {
        let data = {
          room_id: payload.room_id
          
        };

        chatModel
          .find(data)
          .sort()
          .select("message")
          .then((data) => {
            console.log("data => ", data);
            return cb(data);
          })
          .catch((err) => {
            console.log(err, "error");
            return cb(err);
          });
      } catch (error) {
        return cb(error);
      }
      io.emit("receivedMessage", "msg");
    });
  });
};

module.exports = connect;
