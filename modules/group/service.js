const chatRoomModel = require('./schema');

// Creating the Group
module.exports.createNewGroup = (payload, callback) => {
     try {
       
       let data = {
         name: payload.name,
         members: payload.members,
         type: payload.type
       }

        chatRoomModel(data)
        .save()
        .then((data) => {
            return callback(null, data);
          })
          .catch((err) => {
            return callback(err);
          });

    } catch (error) {
        return callback(error)
    }

}

// listing the Group  
module.exports.listNewGroup = (payload, callback) => {
    try {
       chatRoomModel
       .find({isActive: true})
       .sort().select('-__v -createdAt -updatedAt -isActive')
       .then((data) => {
           return callback(null, data);
         })
         .catch((err) => {
           return callback(err);
         });

   } catch (error) {
       return callback(error)
   }
}

// Updating the group updateNewGroup
module.exports.updateNewGroup = (payload, body, callback) => {

    try {
        const id = payload.id;
        console.log(id);

      chatRoomModel
      .findByIdAndUpdate({ _id: id }, { $set: body })
      
      .then((data) => {
        return callback(null, data);
      })
      .catch((err) => {
        return callback(err);
      });

    } catch (error) {
        return callback(error)
    }
}

// Removing the group (soft del)

module.exports.removeNewGroup = (payload,  callback) => {

  try {
    const id = payload.id;
    console.log(id);

  chatRoomModel
  .findByIdAndUpdate({ _id: id }, { $set: {isActive: 'false'} })
  .then((data) => {
    return callback(null, data);
  })
  .catch((err) => {
    return callback(err);
  });

} catch (error) {
    return callback(error)
}
}