const router = require('express').Router();

const { createGroup, listGroup, updateGroup, removeGroup } = require('./controller');


const {verifyUser,} = require('../../middleware/auth');

router.post('/createGroup', verifyUser, createGroup);
router.get('/listGroup', verifyUser, listGroup);
router.put('/updateGroup/:id' , verifyUser,  updateGroup);
router.delete('/removeGroup/:id', verifyUser, removeGroup);


module.exports = router;