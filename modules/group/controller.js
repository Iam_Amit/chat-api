const {createNewGroup, listNewGroup, updateNewGroup ,removeNewGroup } = require('./service');

//create group

module.exports.createGroup = (req, res) => {
    createNewGroup(req.body, (error, response) => {
        if(error) return res.status(500).send({msg: 'Something Went wrong !'})
        res.status(200).send({
            msg: ' Group is successfully created !',
            data: response
        })
    })
}

// listing the group  

module.exports.listGroup = (req, res) => {
    listNewGroup(req.params, (error, response) => {
        if(error) return res.status(500).send({msg: 'Something Went wrong !'})
        res.status(200).send({
            msg: ' Below mention is the list of Group !',
            data: response
        })
    })
}

// Updating the Group
module.exports.updateGroup = (req, res) => {
    updateNewGroup(req.params, req.body, (error, response) => {
        if(error) return res.status(500).send({msg: 'Something Went wrong !'})
        res.status(200).send({
            msg: ' Data is updated successfully   !',
            data: response
        })
    })
}

// Removing Group 
module.exports.removeGroup = (req, res) => {
    removeNewGroup(req.params, (error, response) => {
        if(error) return res.status(500).send({msg: 'Something Went wrong !'})
        res.status(200).send({
            msg: ' Group is removed successfully  !',
            data: response
        })
    })
}

