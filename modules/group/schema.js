const mongoose = require("mongoose");

const groupSchema = new mongoose.Schema(
    {
        name: {
            type: String
        },
        members:{
            type: Array,
        
        },
        type: {
            type: String,
            enum: ['group', 'single'],
            default: 'single'
        },
        isActive:{
            type: Boolean,
            default: true

        },
        socketId:{
            type: String
        },
        // conversationId:{
        //     type: String
        // }
    },
    {
        timestamps: true
    }
);

const room = mongoose.model("Room", groupSchema);

module.exports = room;