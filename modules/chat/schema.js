const mongoose = require('mongoose');
// const { v4: uuidv4 } = require('uuid');
// const users = require('../user/schema')
const chatMsgSchema = new mongoose.Schema(
    {
        from_user: {
            type: mongoose.Schema.Types.ObjectId, ref: 'users'
        },
        to_user: [{
            type: mongoose.Schema.Types.ObjectId, ref: 'users'
            
        }],
        message: {
            type: String
        },
        is_read: {
            type: Boolean,
            default: false
        }, 
        // conversationId: {
        //     type: String
        // }
        room_id: {
            type: mongoose.Schema.Types.ObjectId, ref: 'Rooms'
        },
        type : {
                type: mongoose.Schema.Types.ObjectId, ref: 'Rooms'
        },
        is_remove: {
                    type: Boolean,
                    default: false
                    
        },


    }, {
        timestamps: true

    }
);

const chat = mongoose.model('chats', chatMsgSchema);

module.exports = chat;
