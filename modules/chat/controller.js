const { msgNewSent, msgNewReceived, chatNewConversation, deleteNewMessage} = require('./services');



// msgSent 
module.exports.msgSent = (req, res) => {
    msgNewSent(req.body, (error, response) => {
        if(error) return res.status(500).send({msg: ' Something went wrong !'})
        res.status(200).send({
            msg: 'Msg Sent Successfully  !',
            data: response
        })
    })
}

// msg Received

module.exports.msgReceived = (req, res) => {
    msgNewReceived(req.params,  (error, response) => {
        if(error) return res.status(500).send({msg: ' Something went wrong !'})
        res.status(200).send({
            msg: 'Msg received Successfully  !',
            data: response
        })
    })
}

// User conversation

module.exports.chatConversation = (req, res) => {
    chatNewConversation(req.params, (error, response) => {
        if(error) return res.status(500).send({msg: ' Something went wrong !'})
        res.status(200).send({
            msg: 'Msg received Successfully  !',
            data: response
        })
    })
}

// Delete the user Message 

module.exports.deleteMessage = (req, res) => {
    deleteNewMessage(req.params, (error, response) => {
        if(error) return res.status(500).send({msg: ' Something went wrong !'})
        res.status(200).send({
            msg: 'Msg Removed Successfully  !',
            data: response
        })
    })
}