const router = require('express').Router();

const { msgSent, msgReceived, chatConversation, deleteMessage } = require('./controller');

const {verifyUser} = require('../../middleware/auth');

router.post('/sendMessage',verifyUser, msgSent);
router.get('/receivedMessage/:id',verifyUser, msgReceived)

router.get('/userConversation/:id', verifyUser, chatConversation)

router.delete('/deleteMessage/:id',verifyUser, deleteMessage)

module.exports = router;