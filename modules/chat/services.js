const chatModel = require("./schema");
const chatRoomModel = require("../group/schema");
// const { v4: uuidv4 } = require('uuid');


// Message send
module.exports.msgNewSent = async (payload, callback) => {
  try {
    const roomExist = await chatRoomModel.findById({_id: payload.room_id})
    console.log(roomExist)

    if(!roomExist) {
      throw error('can not chat with yourself');
    }
    else {
      console.log('room Exist')
    
      if(payload.type === 'single'){
      let data1 = {
      from_user: payload.from_user,
      message: payload.message,
      to_user: null,
      room_id: payload.room_id,
      // type: 'single'
      }
      chatModel(data1)
      .save()
      .then((data1) => {
        return callback(null, data1);
      })
      .catch((err) => {
        return callback(err);
      });
    }
    let data2 = {
      from_user: payload.from_user,
      message: payload.message,
      to_user: payload.to_user,
      room_id: payload.room_id,
      // type: payload.type
    };

    chatModel(data2)
      .save()
      .then((data2) => {
        return callback(null, data2);
      })
      .catch((err) => {
        return callback(err);
      });
    }
  } catch (err) {
    return callback(err);
  }
};



// Message Received
module.exports.msgNewReceived = async (payload,  callback) => {
  try {
    const id = payload.id;
    console.log(id);
    // const readme = body
    chatModel
      .find({room_id: id}, {is_remove: false})
      .sort().select('-__v -createdAt -updatedAt -room_id')
      .then((data) => {

        // const final_data =  chatModel.updateMany({is_read: true})
        console.log(data);
        return callback(null, data);
      })
      .catch((err) => {
        return callback(err);
      });

    // const data = await chatModel.find({room_id: id})
    // console.log(data)    


    // const finalData = data.updateMany({is_read: true});
    // console.log(finalData)

    // return callback(null, data);
    
  } catch (err) {
    return callback(err);
  }
};


// User conversation with the different user chatNewConversation

module.exports.chatNewConversation = (payload, callback) => {
  try {
    const id = payload.id;
    console.log(id);

    chatModel
      .find({from_user: id}, )
      .sort().select('-__v -createdAt -updatedAt -_id -is_read')
      .then((data) => {
        console.log(data);
        return callback(null, data);
      })
      .catch((err) => {
        return callback(err);
      });
  } catch (err) {
    return callback(err);
  }
};


// Delete Message API

module.exports.deleteNewMessage = (payload, callback) => {

  try {
    const id = payload.id;
    console.log(id)

    chatModel
    .findByIdAndUpdate({_id: id}, {is_remove: true})
    .then((data) => {
      return callback(null, data);
    })
    .catch((error) => {
      return callback(error)
    })

  } catch (err) {
    
    return callback(err)
  }
}