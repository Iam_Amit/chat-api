const mongoose = require("mongoose");

const userSchema = new mongoose.Schema(
  {
    email: {
      type: String,
    },
    password: {
      type: String,
    },
    userName: {
      type: String
    },
    photo: {
      type: String
    },
    address: {
      type: String
    },
    lat: {
      type: String
    },
    long: {
      type: String
    },
    gender:{
      type: String,
      enum: ['male', 'female', 'others']
    },
    isOnline: {
      type: Boolean
    },
    socketId: {
      type: String
    },
    token: {
      type: String
    }
  },
   {
    timestamps: true,
  }
);

const user = mongoose.model("users", userSchema);

module.exports = user;
