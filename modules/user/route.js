const router = require('express').Router();

const {addUser, userLogin, userList, listUserById, updateProfile, onlineUser, } = require('./controller')

const {verifyUser,} = require('../../middleware/auth');

router.post('/userSignUp', verifyUser,addUser);
router.post('/userLogin', userLogin);

router.get('/listUser', userList);
router.get('/listUser/:id', listUserById)

router.put('/updateProfile/:id', verifyUser, updateProfile)

router.get('/onlineUser', verifyUser, onlineUser)

module.exports = router;
