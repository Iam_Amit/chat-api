const { addNewUser ,userNewLogin ,userNewList ,listNewUserById  ,updateNewProfile, onlineNewUser } = require('./services');


//User Sign up
module.exports.addUser = (req, res) => {
    addNewUser(req.body, (error, response) => {
        if(error) return res.status(500).send({msg: ' Something went wrong !'})
        res.status(200).send({
            msg: 'User Added Successfully !',
            data: response
        })
    })
}


// User Login
module.exports.userLogin = (req, res) => {
    userNewLogin(req.body, (error, response) => {
        if(error) return res.status(500).send({msg: ' Something went wrong !'})
        res.status(200).send({
            msg: 'User Logged In Successfully !',
            data: response
        })
    })
}

// List User

module.exports.userList = (req, res) => {
    userNewList(req.query, (error, response) => {
        if(error) return res.status(500).send({msg: ' Something went wrong !'})
        res.status(200).send({
            msg: 'List of user ',
            data: response
        })
    })
}

// List user by id

module.exports.listUserById = (req, res) => {
    listNewUserById(req.params, (error, response) => {
        if(error) return res.status(500).send({msg: ' Something went wrong !'})
        res.status(200).send({
            msg: 'List of user by Id ',
            data: response
        })
    })
}


// Update the user Profile

module.exports.updateProfile = (req, res) => {
    updateNewProfile(req.params, req.body, (error, response) => {
        if(error) return res.status(500).send({msg: 'Something went wrong !'})
        res.status(200).send({
            msg: ' Profile is updated successfully !',
            data: response
        })
    })
}

// List of Online User

module.exports.onlineUser = (req, res) => {
    onlineNewUser(req.query, (error, response) => {
        if(error) return res.status(500).send({msg: ' Something went wrong !'})
        res.status(200).send({
            msg: 'List of user ',
            data: response
        })
    })
}