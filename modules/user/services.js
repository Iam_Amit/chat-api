const userModel = require("./schema");
const { signToken } = require("../../middleware/auth");
const bcrypt = require("bcrypt");

// User Sign up
module.exports.addNewUser = (payload, callback) => {
  try {
    // console.log(global.sayHi())
    const { userName, email, password } = payload;
    userModel.findOne({ email: email }).then((userData) => {
      if (userData) return callback("Email already taken");
      //password hashing
      let salt;
      let passwordHash = "";
      bcrypt.genSalt(10, (_, output) => {
        salt = output;
        bcrypt.hash(password, salt, (_, hash) => {
          passwordHash = hash;
          const user = userModel({
            userName,
            email,
            password: passwordHash,
          });
          //
          user
            .save()
            .then((data) => {
              const token = signToken(user);
              userModel
                .findOneAndUpdate(
                  { _id: data._id },
                  { token: token },
                  { new: true }
                )
                .then((userToken) => {
                  return callback(null, { data, token });
                })
                .catch((error) => {
                  return callback(error);
                });
            })
            .catch((err) => {
              return callback(err);
            });
        });
      });
    });
  } catch (error) {
    return callback(error);
  }
};

// User Login
module.exports.userNewLogin = async (payload, callback) => {
  try {
    const { email, password } = payload;
    console.log(payload);
    let res = await userModel.findOne({ deletedAt: null, email: email }).exec();

    console.log(res);
    if (res) {
      await bcrypt.compare(password, res.password).then(async (result) => {
        if (result) {
          let token = await signToken(res);
          console.log(token);
          const UpdateUser = await userModel
            .findOneAndUpdate({ email: email }, { token: token }, { new: true })
            .select(
              "-deletedAt -resetPasswordToken -password -createdAt -updatedAt -__v"
            ); //update new token in db

          return callback(null, UpdateUser);
        } else {
          return callback({ msg: "Password didn't match", status: 406 });
        }
      });
    } else {
      callback({ msg: `Your account is blocked !`, status: 404 });
    }
  } catch (error) {
    return callback(error);
  }
};

// List User
module.exports.userNewList = (payload, callback) => {
  try {
    userModel
      .find()
      .sort()
      .select("-email -password -createdAt -updatedAt -__v -address -gender -lat -long -photo")
      .then((data) => {
        return callback(null, data);
      })
      .catch((err) => {
        return callback(err);
      });
  } catch (error) {
    return callback(error);
  }
};

// List the user by Id 

module.exports.listNewUserById = (payload, callback) => {
  try {
    const id = payload.id;
    console.log(id)

    userModel.find({_id: id}).select('-password -__v ')
    .then((data) => {
      return callback(null, data);
    })
    .catch((err) => {
      return callback(err);
    });

  } catch (error) {
    return callback(error)
  }
}

// Update The profile
module.exports.updateNewProfile = (payload, body, callback) => {
  try {
    const id = payload.id;
    console.log(id);

    userModel
      .findByIdAndUpdate({ _id: id }, { $set: body })
      .select("-__v -email -password")
      .then((data) => {
        return callback(null, data);
      })
      .catch((err) => {
        return callback(err);
      });
  } catch (error) {
    return callback(error);
  }
};


// List of Online User 

module.exports.onlineNewUser = (payload, callback) => {
  try {
    userModel
      .find({isOnline: true})
      .sort()
      .select("-email -password -createdAt -updatedAt -__v -address -gender -lat -long -photo -isOnline")
      .then((data) => {
        return callback(null, data);
      })
      .catch((err) => {
        return callback(err);
      });
  } catch (error) {
    return callback(error);
  }
};