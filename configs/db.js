const mongoose = require('mongoose');

const connection = mongoose
.connect(`mongodb://127.0.0.1:27017/Chat_Api`, {
    useNewUrlParser: true,
useUnifiedTopology:true,
useCreateIndex: true,
useFindAndModify: false
}).then(() => {
    console.log(' ************ You Are successfully connected to the DataBase *********** ');
}).catch((err) => {
    console.log('OOPs Something went wrong !', err);
});


module.exports = connection;