
const db = require("./db");
require("dotenv").config();

module.exports.port = process.env.PORT;
module.exports.jwtKey = process.env.JWT_SECRET_KEY;
module.exports.jwtExpireIn = process.env.JWT_EXPIRE_IN;
module.exports.db = db;

