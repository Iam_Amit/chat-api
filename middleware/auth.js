const jwt = require("jsonwebtoken");
const { jwtKey, jwtExpireIn } = require("../configs/configs");
const userSchema = require("../modules/user/schema");

module.exports.verifyUser = async (req, res, next) => {
  let token;
  console.log(111);
  if (!req.headers.hasOwnProperty("authorization"))
    return res.status(401).json({ message: "Unauthorized!" }).end();
  try {
    token = req.headers.authorization.split(" ")[1];
  } catch (e) {
    console.log(222);
    return res.status(500).json({ message: "Internal Server Error" }).end();
  }
  if (!token) {
    console.log(333);
    return res.status(401).json({ message: "Unauthorized!" }).end();
  }
  var payload;
  try {
    const validToken = await userSchema.findOne({ token: token });
    if (!validToken) {
      return res.status(406).json({ msg: " Not Acceptable !" }).end();
    }
    payload = jwt.verify(token, jwtKey);
  } catch (e) {
    console.log(444);
    console.log("Error is =>", e);
    if (e instanceof jwt.JsonWebTokenError) {
      if (e.name == "TokenExpiredError")
        return res.status(401).json({ message: "Session Expired!" }).end();
    }
    return res.status(400).end();
  }
  console.log(555);
  let out = await userSchema.findById(payload._id);
  if (out) {
    const user = out;
    req.body["token_user"] = user;
    // console.log('qwertyu',req.body["token_user"], )
    next();
  } else {
    console.log(666);
    return res.status(401).json({ message: "Unauthorized!" }).end();
  }
};

module.exports.signToken = (payload) => {
  jwtPayload = {
    _id: payload._id,
    Email: payload.Email,
  };
  let expire = jwtExpireIn;
  const token = jwt.sign(jwtPayload, jwtKey, {
    algorithm: "HS256",
    expiresIn: expire,
  });
  return token;
};
